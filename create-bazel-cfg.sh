#!/bin/sh

BASE=`pwd`
. ./config

cat > tensorflow.git/.bazelrc <<EOF
import ${BASE}/tensorflow.git/.tf_configure.bazelrc
EOF

cat > tensorflow.git/tools/python_bin_path.sh <<EOF
export PYTHON_BIN_PATH="${BASE}/lib/bin/python"
EOF

cat > tensorflow.git/.tf_configure.bazelrc <<EOF
build --action_env PYTHON_BIN_PATH="/usr/bin/python3"
build --action_env PYTHON_LIB_PATH="/usr/lib/python3/dist-packages"
build --python_path="/usr/bin/python3"
build --define with_jemalloc=true
build:gcp --define with_gcp_support=true
build:hdfs --define with_hdfs_support=true
build:s3 --define with_s3_support=true
build:kafka --define with_kafka_support=true
build:xla --define with_xla_support=true
build:gdr --define with_gdr_support=true
build:verbs --define with_verbs_support=true
build --action_env TF_NEED_OPENCL_SYCL="0"
build --action_env TF_NEED_CUDA="1"
build --action_env CUDA_TOOLKIT_PATH="/usr/local/cuda-9.2"
build --action_env TF_CUDA_VERSION="9.2"
build --action_env CUDNN_INSTALL_PATH="/usr/lib/x86_64-linux-gnu"
build --action_env TF_CUDNN_VERSION="7"
build --action_env TF_NCCL_VERSION="1"
build --action_env TF_CUDA_COMPUTE_CAPABILITIES="${TF_CUDA_COMPUTE_CAPABILITIES}"
build --action_env LD_LIBRARY_PATH="/usr/local/cuda-9.2/extras/CUPTI/lib64:/usr/local/cuda-9.2/lib64"
build --action_env TF_CUDA_CLANG="0"
build --action_env GCC_HOST_COMPILER_PATH="/usr/bin/gcc-6"
build --config=cuda
test --config=cuda
build --define grpc_no_ares=true
build:opt --copt=-march=native
build:opt --host_copt=-march=native
build:opt --define with_default_optimizations=true
build --strip=always
EOF
