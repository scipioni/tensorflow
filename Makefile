# tensorflow release
RELEASE=r1.9

mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(shell dirname $(mkfile_path))
export PATH := $(current_dir)/lib/bin:$(current_dir):/usr/lib/gcc/x86_64-linux-gnu/6:$(PATH)


lib/bin/activate:
	python -mvenv lib
	lib/bin/pip install -U pip wheel
	lib/bin/pip install -U numpy
    

tensorflow.git:
	git clone -b $(RELEASE) https://github.com/tensorflow/tensorflow tensorflow.git
	bash -c "./create-bazel-cfg.sh"

# required by configure
/usr/lib64:
	sudo ln -s /usr/lib/x86_64-linux-gnu /usr/lib64

bazel: /usr/bin/bazel

/usr/bin/bazel:
	echo "deb [arch=amd64] http://storage.googleapis.com/bazel-apt stable jdk1.8" | sudo tee /etc/apt/sources.list.d/bazel.list
	curl https://bazel.build/bazel-release.pub.gpg | sudo apt-key add -
	sudo apt-get update && sudo apt-get install bazel

/usr/share/doc/libcupti-dev/changelog.Debian.gz:
	sudo apt-get install libcupti-dev
	sudo mkdir -p /usr/extras/CUPTI/include\nsudo mkdir -p /usr/extras/CUPTI/lib64
	sudo ln -s /usr/lib/x86_64-linux-gnu/libcupti.so /usr/extras/CUPTI/lib64/libcupti.so
	sudo ln -s /usr/include/cupti.h /usr/extras/CUPTI/include/cupti.h

#/usr/nvvm: /usr/lib/cuda/nvvm/libdevice
#	sudo ln -s /usr/lib/cuda/nvvm/ /usr/nvvm

#/usr/lib/cuda/nvvm/libdevice:
#	sudo apt install nvidia-cuda-toolkit

/usr/lib/gcc/x86_64-linux-gnu/6/cc1plus:
	sudo apt install g++-6

tools: tensorflow.git/tensorflow/bazel-bin/tensorflow/tools/graph_transforms/transform_graph

tensorflow.git/tensorflow/bazel-bin/tensorflow/tools/graph_transforms/transform_graph:
	bash -c "cd tensorflow.git && bazel build --config=opt --config=cuda tensorflow/tools/graph_transforms:transform_graph"

tensorflow.git/.tf_configure.bazelrc:
	create-bazel-cfg.sh

.PHONY: tensorflow
tensorflow: lib/bin/activate tensorflow.git bazel /usr/lib64 /usr/share/doc/libcupti-dev/changelog.Debian.gz /usr/nvvm tensorflow.git/.tf_configure.bazelrc /usr/lib/gcc/x86_64-linux-gnu/6/cc1plus
	make compile

compile:
	#bash -c "cd tensorflow.git && ./configure"
	bash -c "cd tensorflow.git && bazel build --config=opt --config=cuda //tensorflow/tools/pip_package:build_pip_package"
	bash -c "cd tensorflow.git && bazel-bin/tensorflow/tools/pip_package/build_pip_package .."
	@echo "created wheel package in current dir"
	@ls . *whl

all: tensorflow
