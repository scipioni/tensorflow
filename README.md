# tensorflow compile from sources on ubuntu 18.04 and cuda 9.1

Edit ./config file

Create virtualenv 
```
python3 -mvenv lib
. lib/bin/activate
pip install -U pip wheel numpy
```

Compile
```
make tensorflow
```

My .tf_configure.bazelrc file
```
build --action_env PYTHON_BIN_PATH="/opt/tensorflow/lib/bin/python"
build --action_env PYTHON_LIB_PATH="/opt/tensorflow/lib/lib/python3.6/site-packages"
build --force_python=py3
build --host_force_python=py3
build --python_path="/opt/tensorflow/lib/bin/python"
build --define with_jemalloc=true
build:gcp --define with_gcp_support=true
build:hdfs --define with_hdfs_support=true
build:s3 --define with_s3_support=true
build:kafka --define with_kafka_support=true
build:xla --define with_xla_support=true
build:gdr --define with_gdr_support=true
build:verbs --define with_verbs_support=true
build --action_env TF_NEED_OPENCL_SYCL="0"
build --action_env TF_NEED_CUDA="1"
build --action_env CUDA_TOOLKIT_PATH="/usr"
build --action_env TF_CUDA_VERSION="9.1"
build --action_env CUDNN_INSTALL_PATH="/usr/lib/cuda"
build --action_env TF_CUDNN_VERSION="7"
build --action_env TF_NCCL_VERSION="1"
build --action_env TF_CUDA_COMPUTE_CAPABILITIES="3.5,5.0"
build --action_env TF_CUDA_CLANG="0"
build --action_env GCC_HOST_COMPILER_PATH="/usr/bin/gcc-6"
build --config=cuda
test --config=cuda
build --define grpc_no_ares=true
build:opt --copt=-march=native
build:opt --host_copt=-march=native
build:opt --define with_default_optimizations=true
build --copt=-DGEMMLOWP_ALLOW_SLOW_SCALAR_FALLBACK
build --host_copt=-DGEMMLOWP_ALLOW_SLOW_SCALAR_FALLBACK

```
